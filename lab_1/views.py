from django.shortcuts import render
from datetime import datetime

# Enter your name here
mhs_name = 'Ilham Rizky Akbar' # TODO Implement this
mhs_born = 1998
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(mhs_born)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    thisYear = datetime.now().year
    age = thisYear - birth_year
    return age
